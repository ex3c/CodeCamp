﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;

public class DungeonGenerator : MonoBehaviour
{

    public int width = 200;
    public int height = 200;

    public int roomWidthMin = 8;
    public int roomWidthMax;
    public int roomHeightMin = 8;
    public int roomHeigthMax;
    public int wallHeight = 5;

    public string seed = "0";
    public bool useSeed;
    public bool useRandomSeed;
    public bool alt;
    public bool smooth;

    public bool mapCreated = false;
    public int xStart;
    public int zStart;

    //World world = GameObject.FindObjectOfType<World>();

    //[Range(0, 50)]
    //public int roomDensity = 15;

    [Range(1, 10)]
    public int passagewidth;


    [Range(0, 100)]
    public int randomFillPercent;   // Bestimmt prozentuale Füllung der Map

    int[,] map;
    int[, ,] mapDome;

    void Start()
    {
        Debug.Log("  Started   ");
        GenerateMap(0, 0);
    }

    //Beim Mausklick in der Game-View eine neue Map erstellen.
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //GenerateMap();
        }
    }

    public void GenerateMap(int xCoord, int zCoord)
    {
        map = new int[width, height];
        if (alt)
        {
            RandomFillMap();
        }
        else
        {
            FillMap();
        }

        if (smooth)
        {
            for (int i = 0; i < 5; i++)
            {
                SmoothMap();
            }
        }

        ProcessMap();

        xStart = xCoord;
        zStart = zCoord;

        mapCreated = true;
        CreateDome();
    }

    public bool isOnMap(int xCoord, int zCoord)
    {
        if (xStart <= xCoord && xCoord < (xStart + map.GetLength(0)))
        {
            if (zStart <= zCoord && zCoord < (zStart + map.GetLength(1)))
            {
                return true;
            }
        }

        return false;
    }

    void CreateDome()
    {
        //World world = GameObject.FindObjectOfType<World>();
        mapDome = new int[width, wallHeight, height];

        for (int y = 0; y < wallHeight; y++)
        {
            for (int x = 0; x < map.GetLength(0)-1; x++)
            {
                for (int z = 0; z < map.GetLength(1)-1; z++)
                {
                    if (y == 0)
                    {
                        mapDome[x, y, z] = map[x, z];
                    }
                    else
                    {
                        if (mapDome[x, y, z] == 0)
                        {
                            if (x == 0 | z == 0)
                            {
                                mapDome[x, y, z] = 1;
                            }
                            else if (mapDome[x - 1, y - 1, z - 1] == 1 | mapDome[x - 1, y - 1, z + 1] == 1 | mapDome[x + 1, y - 1, z - 1] == 1 | mapDome[x + 1, y - 1, z + 1] == 1)
                            {
                                mapDome[x, y, z] = 1;
                            }
                            else
                            {
                                mapDome[x, y, z] = 0;
                            }
                        }
                    }
                }
            }
        }
    }


    public void DungeonChunk(int xCoord, int yCoord, int zCoord)
    {
        World world = GameObject.FindObjectOfType<World>();
        for (int y = -1; y < wallHeight; y++)
        {
            for (int x = xCoord; x < xCoord + Chunk.chunkSize; x++)
            {
                for (int z = zCoord; z < zCoord + Chunk.chunkSize; z++)
                {
                    if (x < (xStart + map.GetLength(0)) && z < (zStart + map.GetLength(1)))
                    {
                        if (y == -1)
                        {
                            world.SetBlock(x, y + yCoord, z, new BlockWood());
                        }
                        else if (map[x - xStart, z - zStart] == 1)
                        {
                            world.SetBlock(x, y + yCoord, z, new Block());
                        }
                        else
                        {
                            world.SetBlock(x, y + yCoord, z, new BlockAir());
                        }
                    }
                    else
                    {
                        world.SetBlock(x, y + yCoord, z, new BlockAir());
                    }
                }
            }
        }
        DomeChunk(xCoord, yCoord + wallHeight, zCoord);
    }

    public void DomeChunk(int xCoord, int yCoord, int zCoord)
    {
        World world = GameObject.FindObjectOfType<World>();
        for (int y = 0; y < mapDome.GetLength(1); y++)
        {
            for (int x = xCoord; x < xCoord + Chunk.chunkSize; x++)
            {
                for (int z = zCoord; z < zCoord + Chunk.chunkSize; z++)
                {
                    if (x < (xStart + mapDome.GetLength(0)) && z < (zStart + mapDome.GetLength(2)))
                    {
                        if (mapDome[x - xStart,y , z - zStart] == 1)
                        {
                            world.SetBlock(x, y + yCoord, z, new Block());
                        }
                        else
                        {
                            world.SetBlock(x, y + yCoord, z, new BlockAir());
                        }
                    }
                    else
                    {
                        world.SetBlock(x, y + yCoord, z, new BlockAir());
                    }
                }
            }
        }
    }

    void GenerateMap()
    {
        map = new int[width, height];
        //RandomFillMap();
        FillMap();

        /*for (int i = 0; i < 5; i++)
        {
            SmoothMap();
        }*/

        ProcessMap();

        World world = GameObject.FindObjectOfType<World>();

        for (int y = -1; y < 5; y++)
        {
            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int z = 0; z < map.GetLength(1); z++)
                {
                    //Chunk chunk = world.GetChunk(x, -10, z);
                    if (y == -1)
                    {
                        world.SetBlock(x - 100, 50 + y, z - 100, new BlockWood());
                    }
                    else if (map[x, z] == 1)
                    {
                        world.SetBlock(x - 100, 50 + y, z - 100, new Block());
                    }
                    else
                    {
                        world.SetBlock(x - 100, 50 + y, z - 100, new BlockAir());
                    }
                }
            }
        }


        /*int borderSize = 1; // Dicke des Randes der Map
        int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

        for (int x = 0; x < borderedMap.GetLength(0); x++)
        {
            for (int y = 0; y < borderedMap.GetLength(1); y++)
            {
                if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize)    // Wenn wir innerhalb der Map sind
                {
                    borderedMap[x, y] = map[x - borderSize, y - borderSize];
                }
                else    // Wenn wir nicht in der Map sind, sondern in der Bordered-Area
                {
                    borderedMap[x, y] = 1;
                }
            }
        }

        MeshGenerator meshGen = GetComponent<MeshGenerator>();
        meshGen.GenerateMesh(borderedMap, 1);*/
    }

    // Jede Region die aus weniger als 50 Kacheln besteht, wird von der Map entfernt.
    // Diese Methode ist notwendig um einzelne "Wand Fragmente" die im Raum stehen bleiben würden, zu entfernen.
    void ProcessMap()
    {
        List<List<Coord>> wallRegions = GetRegions(1);
        int wallThresholdSize = 50; // Nur Wand-Fragmente mit weniger als 50 Kacheln entfernen. 
        // Größere Regionen eignen sich als Felsen oder Säulen innerhalb eines Raumes.

        foreach (List<Coord> wallRegion in wallRegions) //... also jede Region...
        {
            if (wallRegion.Count < wallThresholdSize)   // ...auf Schwellwert von 50 prüfen...
            {
                foreach (Coord tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = 0;    //... und ggf. entfernen.
                }
            }
        }

        // Ähnliches Verfahren mit Räumen - um mehrere kleinere/größere Räume zu erstellen.
        List<List<Coord>> roomRegions = GetRegions(0);
        int roomThresholdSize = 50;
        List<Room> survivingRooms = new List<Room>();   // Liste von Räumen, die alle die Entfernung der Räume unter 50 überlebt haben.

        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coord tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = 1;
                }
            }
            else
            {
                survivingRooms.Add(new Room(roomRegion, map));  // Hat der geprüfte Raum überlebt, füge ihn hinzu.
            }
        }
        survivingRooms.Sort();
        survivingRooms[0].isMainRoom = true;
        survivingRooms[0].isAccessibleFromMainRoom = true;

        ConnectClosestRooms(survivingRooms);
    }

    void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
    {

        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();

        if (forceAccessibilityFromMainRoom) // Wenn Räume gezwungen werden, Wege zu bekommen...
        {
            foreach (Room room in allRooms) // ...Nochmal alle durchgehen...
            {
                if (room.isAccessibleFromMainRoom)  // ... Wenn davon der aktuelle zugänglich ist...
                {
                    roomListB.Add(room);    // den Main-Room mit Raum B verbinden...
                }
                else
                {
                    roomListA.Add(room);    //... sonst mit Raum A.
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int bestDistance = 0;   // kürzeste Distanz.
        Coord bestTileA = new Coord();  // näheste Kachel in Raum A
        Coord bestTileB = new Coord();  // näheste Kachel in Raum B
        Room bestRoomA = new Room();    // erster verglichener Raum
        Room bestRoomB = new Room();    // zweiter verglichener Raum
        bool possibleConnectionFound = false;   // Standardmäßig gibt es keinen kürzesten Weg

        // Durch alle Räume gehen und die Distanz zwischen allen Kacheln (am Rand) in Raum A und Raum B bestimmen.
        foreach (Room roomA in roomListA)
        {
            if (!forceAccessibilityFromMainRoom)
            {
                possibleConnectionFound = false;
                if (roomA.connectedRooms.Count > 0) // Wenn noch keine Verbindung gefunden...
                {
                    continue;   //... weiter suchen.
                }
            }

            foreach (Room roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB)) // Wenn Raum A, Raum B ist , muss keine Verbindung gefunden werden. | Oder: Ist Raum A schon mit Raum B verbunden...
                {
                    continue;
                }

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
                    {
                        Coord tileA = roomA.edgeTiles[tileIndexA];
                        Coord tileB = roomB.edgeTiles[tileIndexB];

                        int distanceBetweenRooms = (int)(Mathf.Pow(tileA.tileX - tileB.tileX, 2) + Mathf.Pow(tileA.tileY - tileB.tileY, 2));

                        // Distanzen vergleichen. | kürzeste Distanz bestimmen
                        if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
                        {
                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            // Fälle absichern, um Räume zu zwingen sich zu verbinden.
            // Dies verhindert Isolation (Sicherstellung, dass jeder Raum mit einem mind. einem anderen verbunden ist)
            if (possibleConnectionFound && !forceAccessibilityFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (possibleConnectionFound && forceAccessibilityFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(allRooms, true);
        }

        // Räume die immer noch keine Verbindung haben, zwingen sich zu verbinden
        if (!forceAccessibilityFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB)
    {
        Room.ConnectRooms(roomA, roomB);

        // Verbundene Wege durch Linie visualisieren | 10 Sekunden anzeigbar.
        Debug.DrawLine(CoordToWorldPoint(tileA), CoordToWorldPoint(tileB), Color.green, 10);

        List<Coord> line = GetLine(tileA, tileB);
        foreach (Coord c in line)
        {
            DrawCircle(c, passagewidth);
        }
    }

    // Kreise zum ausstanzen der Wege bilden. | Erhält Koordinate und Radius
    void DrawCircle(Coord c, int r)
    {
        for (int x = -r; x <= r; x++)
        {
            for (int y = -r; y <= r; y++)
            {
                if (x * x + y * y <= r * r)
                {
                    int drawX = c.tileX + x;
                    int drawY = c.tileY + y;
                    if (IsInMapRange(drawX, drawY)) // wenn die Koordinate innerhalb der Map ist.
                    {
                        map[drawX, drawY] = 0;  // Wand entfernen (ausstanzen)
                    }
                }
            }
        }
    }

    // Methode mit Algorithmus, um Wege auszuhöhlen (Ursprüngliche Debuglinien werden zu eigentlichen Wegen)
    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();

        int x = from.tileX;
        int y = from.tileY;

        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;

        bool inverted = false;  // Zur Prüfung, ob Weg invertiert dargestellt wird.
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if (longest < shortest) // Wenn Weg invertiert ist...
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));

            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }

        return line;
    }

    Vector3 CoordToWorldPoint(Coord tile)
    {
        return new Vector3(-width / 2 + .5f + tile.tileX, 2, -height / 2 + .5f + tile.tileY);
    }

    // Methode bekommt Kacheltyp und gibt Region des Kacheltyps zurück
    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coord> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Coord tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;   // Jede überprüfte Kachel als "überprüft" makieren.
                    }
                }
            }
        }

        return regions;
    }

    // Die komplette Map in Kachelschritten prüfen und Koordinaten bestimmen.
    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();  //Liste von Koordinaten um Kacheln zu sichern.
        int[,] mapFlags = new int[width, height];   //Merken welche Kacheln wir schon überprüft haben.
        int tileType = map[startX, startY]; // Kacheln kategorisieren in Wand und Leerstelle (1 und 0)

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));   //Startkoordinaten
        mapFlags[startX, startY] = 1;   // Schon geprüfte Kachel

        while (queue.Count > 0) // Bis alle items geprüft wurden.
        {
            Coord tile = queue.Dequeue();   // Erstes item der Queue
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coord(x, y)); // Neue Koordinate hinzufügen
                        }
                    }
                }
            }
        }

        return tiles;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }


    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();    //zufälligen Seed anhand der Systemzeit.
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Der Rand der Karte soll immer aus Wand bestehen. | Löcher am Rand sollen vermieden werden.
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)  // Bedingungen wann eine Wand gezeichnet wird.
                {
                    map[x, y] = 1;  //ausgefüllte Wand
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;    // Wenn die Kachel zwischen 0 und 100 ist 
                    // (innerhalb unserer Füllung), 
                    // füge ein Wand hinzu 
                    // sonst lasse die Stelle leer. 
                }
            }
        }
    }

    void FillMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                map[x, y] = 1;
            }
        }
        GenerateDungeon();
    }


    void GenerateDungeon()
    {
        System.Random rnd;
        if (useSeed)
        {
            rnd = new System.Random(seed.GetHashCode());
        }

        else
        {
            rnd = new System.Random();
        }

        int roomDensity = Convert.ToInt32(0.05 * width + 0.05 * height);
        roomWidthMax = Convert.ToInt32(0.25 * width);
        roomHeigthMax = Convert.ToInt32(0.25 * height);

        for (int i = 0; i < roomDensity; i++)
        {

            int xWalls = rnd.Next(roomWidthMin, roomWidthMax);
            int yWalls = rnd.Next(roomHeightMin, roomHeigthMax);

            int xStart = rnd.Next(1, width - xWalls);
            int yStart = rnd.Next(1, height - yWalls);

            for (int x = xStart; x < xStart + xWalls; x++)
            {
                for (int y = yStart; y < yStart + yWalls; y++)
                {
                    map[x, y] = 0;
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    map[x, y] = 1;
                else if (neighbourWallTiles < 4)
                    map[x, y] = 0;

            }
        }
    }

    //Zählen wie viele andere Kacheln um einer Kachel liegen.
    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;

        // X Nachbarn
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            // Y Nachbar
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))   //Wenn wir mit Sicherheit in der Map sind...
                {
                    if (neighbourX != gridX || neighbourY != gridY) // ...wenn die Nachbar-Kacheln ausgefüllte Wände sind...
                    {
                        wallCount += map[neighbourX, neighbourY];   // ...Kachel zählen
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    // Struktur der Koordinaten
    // Zur örtlichen Bestimmung der Kacheln (nötig für Algorithmen).
    struct Coord
    {
        public int tileX;
        public int tileY;

        public Coord(int x, int y)
        {
            tileX = x;
            tileY = y;
        }
    }


    // Isolierte Räume werden mit Wegen miteinander verbunden. 
    // Es wird immer die kürzeste Distanz zum Raum gesucht, um einen Weg zu erstellen.
    class Room : IComparable<Room>
    {
        public List<Coord> tiles;
        public List<Coord> edgeTiles;
        public List<Room> connectedRooms;   /* Räume die eine gemeinsame Passage teilen
                                             * Bsp.: A--B--C
                                             * Eine Gemeinsame Passage ist zwischen A und B. Aber nicht zwischen A und C*/
        public int roomSize;    // Räume sollen nach Größe sortiert werden.

        public Room()   // Leere Methode, damit beim Raumvergleich ein leerer Raum verglichen werden kann.
        {

        }

        public bool isAccessibleFromMainRoom;
        public bool isMainRoom;

        public Room(List<Coord> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Room>();

            edgeTiles = new List<Coord>();

            // Durch alle Kacheln in dem Raum gehen.
            foreach (Coord tile in tiles)
            {
                for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)  // Alle Kachel-Nachbarn anschauen. | Wenn ein Nachbar eine Wand-Kachel ist, wissen wir, dass wir am Rand des Raumes sind.
                {
                    for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                    {
                        if (x == tile.tileX || y == tile.tileY) // Wir prüfen nur die Nachbarn links, rechts und oben, unten. - keine Diagonalen.
                        {
                            if (map[x, y] == 1)
                            {
                                edgeTiles.Add(tile);
                            }
                        }
                    }
                }
            }
        }

        // Erreichbare Räume zu den verbundenden Räumen hinzufügen
        public void SetAccessibleFromMainRoom()
        {
            if (!isAccessibleFromMainRoom)
            {
                isAccessibleFromMainRoom = true;
                foreach (Room connectedRoom in connectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        // Räume verbinden.
        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if (roomA.isAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.isAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }
            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }

        public int CompareTo(Room otherRoom)    // Für das Comparable Interface
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }
    }




#if UNITY_EDITOR
    [MenuItem("GameObject/PGVW/Dungeon")]
    public static DungeonGenerator CreateMyDungeon()
    {
        GameObject gO = new GameObject("Dungeon");
        DungeonGenerator dungeon = gO.AddComponent<DungeonGenerator>();

        return dungeon;
    }
#endif
}