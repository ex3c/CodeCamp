﻿using UnityEngine;
using UnityEditor;
using System.Collections;
/*Custom Editor für den Dungeon-Generator: 
 * 
 *Enthält Dungeon-Settings(Stand:08.02)
 *-Höhe und Breite des Dungeons
 *-generierte Seeds
 *-Füllvarianten für den Dungeon
 *-Einstellbare Gangbreite zwischen den Räumen
 *
 */


[CustomEditor(typeof(DungeonGenerator))]
public class DungeonCreatorEditor : Editor
{

    DungeonGenerator dungeon;

    void Awake()
    {
        dungeon = target as DungeonGenerator;
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();


        //Einstellungsmöglichkeiten für den zu generierenden Dungeon
        #region Dungeon-Settings
        EditorGUILayout.LabelField("Dungeon-Settings", EditorStyles.boldLabel);
        EditorGUILayout.BeginVertical("Box");
        dungeon.width = EditorGUILayout.IntSlider(
            "Width", dungeon.width, 100, 700);

        dungeon.height = EditorGUILayout.IntSlider(
            "Height", dungeon.height, 100, 700);

        dungeon.seed = EditorGUILayout.TextField(
            "Seed", dungeon.seed);


        dungeon.wallHeight = EditorGUILayout.IntSlider(
            "Wallheight", dungeon.wallHeight, 3, 10);

        dungeon.passagewidth = EditorGUILayout.IntSlider(
            "Passagewidth", dungeon.passagewidth, 1, 10);

        dungeon.useSeed = EditorGUILayout.Toggle(
            "Use Seed", dungeon.useSeed);

        dungeon.alt = EditorGUILayout.Toggle(
            "Natural Mode", dungeon.alt);

        dungeon.smooth = EditorGUILayout.Toggle(
            "Map absmooofen", dungeon.smooth);


        dungeon.randomFillPercent = EditorGUILayout.IntSlider(
            "RandomFillPercent", dungeon.randomFillPercent, 0, 100);


        #endregion // dungeon-settings
    }
}
