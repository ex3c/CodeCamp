﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class BlockFlower : BlockIceFlower {

    //override Texture Position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 0;
                tile.y = 1;
                return tile;
            case Direction.down:
                tile.x = 0;
                tile.y = 1;
                return tile;
        }

        tile.x = 13;
        tile.y = 14;


        return tile;
    }
}
