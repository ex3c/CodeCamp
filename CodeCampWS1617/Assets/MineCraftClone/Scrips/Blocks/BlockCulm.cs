﻿using UnityEngine;
using System.Collections;
using System;

public class BlockCulm : BlockIceFlower {

    //override Texture Position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 0;
                tile.y = 1;
                return tile;
            case Direction.down:
                tile.x = 0;
                tile.y = 1;
                return tile;
        }

        tile.x = 11;
        tile.y = 10;

        return tile;
    }
}
