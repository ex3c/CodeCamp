﻿using UnityEngine;
using System.Collections;

public class BlockLSD : Block {

    //override Texture Position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        int x = Random.Range(1, 2);
        int y = Random.Range(2, 8);

        tile.x = x;
        tile.y = y;

        return tile;
    }
}
