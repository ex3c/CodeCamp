﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BlockIceFlower : BlockWater
{
    public BlockIceFlower()
        : base()
    {
        waterResistant = false;
    }
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 0;
                tile.y = 1;
                return tile;
            case Direction.down:
                tile.x = 0;
                tile.y = 1;
                return tile;
        }

        tile.x = 12;
        tile.y = 14;

        return tile;
    }


    /*FaceData methods to change the arrangement:
     * create a Cross of two squares
     * FaceDataUp and FaceDataDown are empty
     * */
    #region FaceData methods
    protected override MeshData FaceDataUp
       (Chunk chunk, int x, int y, int z, MeshData meshData)
    {

        return meshData;
    }

    protected override MeshData FaceDataDown
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {

        return meshData;
    }

    protected override MeshData FaceDataNorth
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z ));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z ));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z ));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.north));
        return meshData;
    }

    protected override MeshData FaceDataEast
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x , y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x , y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x , y - 0.5f, z + 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.east));
        return meshData;
    }

    protected override MeshData FaceDataSouth
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z ));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z ));
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z ));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.south));
        return meshData;
    }

    protected override MeshData FaceDataWest
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x , y - 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x , y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x , y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x , y - 0.5f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.west));
        return meshData;
    }

    #endregion //FaceData methods
}
