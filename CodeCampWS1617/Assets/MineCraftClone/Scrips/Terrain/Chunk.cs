﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class Chunk : MonoBehaviour
{
    #region Fields

    //Array of Blocks in one chunk
    public Block[, ,] blocks = new Block[chunkSize, chunkSize, chunkSize];

    //Size of chunk
    public static int chunkSize = 16;

    //Properties of chunk
    public bool update = false;
    public bool rendered;

    //MeshFilter and MeshCollider of chunk
    MeshFilter filter;
    MeshCollider coll;

    //Reference of World and WorldPos
    public World world;
    public WorldPos pos;

    public List<WorldPos> seas = new List<WorldPos>();

    #endregion Fields

    #region MonoBehaviour Methods

    void Start()
    {
        filter = gameObject.GetComponent<MeshFilter>();
        coll = gameObject.GetComponent<MeshCollider>();
    }

    //Update is called once per frame
    void Update()
    {
        if (update)
        {
            update = false;
            UpdateChunk();
        }
    }

    #endregion MonoBehaviour Methods

    /*
     * Returns block if position in chunk in range
     * Else recursive call of GetBlock in world
     */
    public Block GetBlock(int x, int y, int z)
    {
        if (InRange(x) && InRange(y) && InRange(z))
            return blocks[x, y, z];
        return world.GetBlock(pos.x + x, pos.y + y, pos.z + z);
    }
    
    /*
     * Place a block to a Position 
     * if position is in range
     */
    public void SetBlock(int x, int y, int z, Block block)
    {
        if (InRange(x) && InRange(y) && InRange(z))
        {
            blocks[x, y, z] = block;
        }
        else
        {
            world.SetBlock(pos.x + x, pos.y + y, pos.z + z, block);
        }
    }

    public void SetBlocksUnmodified()
    {
        foreach (Block block in blocks)
        {
            block.changed = false;
        }
    }

    // Updates the chunk based on its contents
    void UpdateChunk()
    {
        rendered = true;
        MeshData meshData = new MeshData();

        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    meshData = blocks[x, y, z].Blockdata(this, x, y, z, meshData);
                }
            }
        }

        RenderMesh(meshData);
    }

    // Sends the calculated mesh information
    // to the mesh and collision components
    void RenderMesh(MeshData meshData)
    {
        filter.mesh.Clear();

        //Fill Vertices and Triangles
        filter.mesh.vertices = meshData.vertices.ToArray();
        filter.mesh.triangles = meshData.triangles.ToArray();

        //Fill UVs
        filter.mesh.uv = meshData.uv.ToArray();
        filter.mesh.RecalculateNormals();

        //CreateMesh
        coll.sharedMesh = null;
        Mesh mesh = new Mesh();
        mesh.vertices = meshData.colVertices.ToArray();
        mesh.triangles = meshData.colTriangles.ToArray();
        mesh.RecalculateNormals();
        coll.sharedMesh = mesh;
    }

    #region Helpermethods

    //Check if x, y or z in range of a given chunksize
    public static bool InRange(int index)
    {
        if (index < 0 || index >= chunkSize)
            return false;

        return true;
    }

    #endregion Helpermethods
}