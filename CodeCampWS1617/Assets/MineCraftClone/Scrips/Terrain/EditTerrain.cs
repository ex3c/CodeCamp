﻿using UnityEngine;
using System.Collections;

public static class EditTerrain
{

    /*
     * Create a WorldPos variable from the rounded components of the vector3 
     * but it will be handy to have this function available anywhere
     */
    public static WorldPos GetBlockPos(Vector3 pos)
    {
        WorldPos blockPos = new WorldPos(
            Mathf.RoundToInt(pos.x),
            Mathf.RoundToInt(pos.y),
            Mathf.RoundToInt(pos.z)
            );

        return blockPos;
    }

    /*
     * Overload of GetBlockPos(Vector3 pos)
     * Use RaycastHit and adjacent which decide which decides 
     * if you should get the block you hit 
     * or the block adjacent to the face you hi
     */
    public static WorldPos GetBlockPos(RaycastHit hit, bool adjacent = false)
    {
        Vector3 pos = new Vector3(
            MoveWithinBlock(hit.point.x, hit.normal.x, adjacent),
            MoveWithinBlock(hit.point.y, hit.normal.y, adjacent),
            MoveWithinBlock(hit.point.z, hit.normal.z, adjacent)
            );

        return GetBlockPos(pos);
    }

    /*
     * MoveWithinBlock gets called with x, y or z and the haycastHit's normal's x y or z
     * If the block is halfway between two blocks it will have a decimal of 0.5 so rounding it 
     * and subtracting the the rounded value from the original value which leaves us with the decimals 
     * Then if we find that it's 0.5 we can use the normal to move it
     * We can use the normal to move the point outwards or inwards, 
     * if we're getting the adjacent block add half the normal to the position pushing it outwards 
     * Only add half because the whole thing could equal up to 1 which would push the position too far back. 
     * If we're not looking for the adjacent block subtract the same amount moving it further into the block we're pointing at
     */
    static float MoveWithinBlock(float pos, float norm, bool adjacent = false)
    {
        if (pos - (int)pos == 0.5f || pos - (int)pos == -0.5f)
        {
            if (adjacent)
            {
                pos += (norm / 2);
            }
            else
            {
                pos -= (norm / 2);
            }
        }

        return (float)pos;
    }

    //Takes a raycastHit and gets the chunk git
    public static bool SetBlock(RaycastHit hit, Block block, bool adjacent = false)
    {
        Chunk chunk = hit.collider.GetComponent<Chunk>();
        if (chunk == null)
            return false;

        WorldPos pos = GetBlockPos(hit, adjacent);

        chunk.world.SetBlock(pos.x, pos.y, pos.z, block);

        return true;
    }

    public static bool SetBlock(int x, int y, int z, RaycastHit hit, Block block)
    {
        Chunk chunk = hit.collider.GetComponent<Chunk>();
        if (chunk == null)
            return false;

        chunk.world.SetBlock(x, y, z, block);

        return true;
    }

    //Return the block hit
    public static Block GetBlock(RaycastHit hit, bool adjacent = false)
    {
        Chunk chunk = hit.collider.GetComponent<Chunk>();
        if (chunk == null)
            return null;

        WorldPos pos = GetBlockPos(hit, adjacent);

        Block block = chunk.world.GetBlock(pos.x, pos.y, pos.z);

        return block;
    }

    public static Block GetBlock(RaycastHit hit, int x, int y, int z)
    {
        Chunk chunk = hit.collider.GetComponent<Chunk>();
        if (chunk == null)
            return null;

        Block block = chunk.world.GetBlock(x, y, z);

        return block;
    }    
}