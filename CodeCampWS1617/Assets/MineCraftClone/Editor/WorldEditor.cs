﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using System.Text.RegularExpressions;

[CustomEditor(typeof(World))]
public class Worldeditor : Editor
{

    World world;
    Texture2D background;

    private static Random.State seedGenerator;
    private static int seedGeneratorSeed = 1337;
    private static bool seedGeneratorInitialized = false;

    void Awake()
    {
        world = target as World;
    }    

    public override void OnInspectorGUI()
    {
        // Load some Textures to make it look better
        string texture = "Editor/WorldGen";
        Texture2D background = Resources.Load(texture) as Texture2D;
        Texture2D background2 = Resources.Load("Editor/bg") as Texture2D;
        Rect rec = new Rect(0, 119, Screen.width, 300);
        GUIStyle texttStyle = new GUIStyle(EditorStyles.boldLabel);
        GUIStyle inputStyle = new GUIStyle(EditorStyles.objectField);
        texttStyle.normal.textColor = Color.white;
        inputStyle.fixedWidth = 200;

        // Just some spaces to make the Image apeal more.
        #region Spaces
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        #endregion

        // Vertical Box Containing all our World Generation 
        EditorGUILayout.BeginVertical("Box");
        GUI.DrawTexture(rec, background);
        GUI.DrawTexture(new Rect(0, 190, Screen.width, 200), background2);
        EditorGUILayout.Space();

        #region Worldname
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Worldname:", texttStyle);
            world.worldName = EditorGUILayout.TextField("", world.worldName, inputStyle);
        EditorGUILayout.EndHorizontal();
        #endregion

        #region ChunkPrefab
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Chunk Prefab:", texttStyle);
            world.chunkPrefab = EditorGUILayout.ObjectField("", world.chunkPrefab, typeof(GameObject), true, GUILayout.Width(200), GUILayout.MaxWidth(200)) as GameObject;

        EditorGUILayout.EndHorizontal();
        #endregion

        #region WorldSeed
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("World Seed:", texttStyle);
            world.seed = EditorGUILayout.LongField("", world.seed, inputStyle);
        EditorGUILayout.EndHorizontal();
        

        if (GUILayout.Button("Generate seed"))
        {
            world.seed = GenerateSeed() + Mathf.Abs(GenerateSeed());
        }
        #endregion

        EditorGUILayout.Space();

        #region WorldType
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("World Type:", texttStyle);
            world.type = (TerrainGen.TYPES)EditorGUILayout.EnumPopup("", world.type);
        EditorGUILayout.EndHorizontal();
        #endregion

        #region WorldBiome
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("World Biome:", texttStyle);
            world.biome = (TerrainGen.BIOMES)EditorGUILayout.EnumPopup("", world.biome);
        EditorGUILayout.EndHorizontal();
        #endregion

        EditorGUILayout.EndVertical();
    }

    public static int GenerateSeed()
    {
        // remember old seed
        var temp = Random.state;

        // initialize generator state if needed
        if (!seedGeneratorInitialized)
        {
            Random.InitState(seedGeneratorSeed);
            seedGenerator = Random.state;
            seedGeneratorInitialized = true;
        }

        // set our generator state to the seed generator
        Random.state = seedGenerator;
        // generate our new seed
        var generatedSeed = Random.Range(int.MinValue, int.MaxValue);
        // remember the new generator state
        seedGenerator = Random.state;
        // set the original state back so that normal random generation can continue where it left off
        Random.state = temp;

        return generatedSeed;
    }

}